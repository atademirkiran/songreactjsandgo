import React from 'react';
import axios from 'axios';


class FormSarki extends React.Component {

    state={
        song:"",
        singer:"",
        musictype:""
    }

    handleChangeSong=(event)=>{
        this.setState({
            song:event.target.value
        })
    }
    handleChangeSinger=(event)=>{
        this.setState({
            singer:event.target.value
        })
    }
    handleChangeType=(event)=>{
        this.setState({
            musictype:event.target.value
        })
    }
    handlePut=(event)=>{
        event.preventDefault();

        axios.post(`http://localhost:8080/api/songs`, { 
 
            song:this.state.song,
            singer:this.state.singer,
            musictype:this.state.musictype},{
         headers: {
           "Content-Type": "application/x-www-form-urlencoded",
         },
       })
    
    }
    render() {
        return (
            <div className="container mt-5">

                <form className="form-group" onSubmit={this.handlePut}>
                    <div className="row">

                        
                        <input className="form-control-s col-3" placeholder="Sarki adi" onChange={this.handleChangeSong} type="text"></input>
                        <input className="form-control-s col-3" placeholder="Sarkici" onChange={this.handleChangeSinger}  type="text"></input>
                        <input className="form-control-s col-3" placeholder="Sarki tur" onChange={this.handleChangeType} type="text"></input>
                    </div>

                    <button type="submit" className="btn btn-outline-success mt-3">Button</button>
                </form>

            </div>
        )
    }
}
export default FormSarki;