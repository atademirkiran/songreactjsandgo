package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type Song struct {
	ID        primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	Song      string             `json:"song" bson:"song"`
	Singer    string             `json:"singer" bson:"singer"`
	MusicType string             `json:"musictype" bson:"musictype"`
}

func ConnectDB() *mongo.Collection {

	clientOptions := options.Client().ApplyURI("mongodb://localhost:27017")

	client, err := mongo.Connect(context.TODO(), clientOptions)

	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Connected to MongoDB!")

	collection := client.Database("sarkiDatabase").Collection("songs")

	return collection
}
func postSongs(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "POST")
	w.Header().Set("Access-Control-Allow-Headers", "*")

	var song Song
	_ = json.NewDecoder(r.Body).Decode(&song)
	collection := ConnectDB()
	result, err := collection.InsertOne(context.TODO(), song)
	if err != nil {
		log.Fatal(err)
		return
	}

	json.NewEncoder(w).Encode(result)

}

func main() {
	r := mux.NewRouter()
	r.HandleFunc("/api/songs", postSongs).Methods("POST")
	log.Fatal(http.ListenAndServe(":8080", r))
}
