module goproj

go 1.13

require (
	github.com/BurntSushi/toml v0.3.1 // indirect
	github.com/gofiber/fiber v1.14.6 // indirect
	github.com/gorilla/mux v1.8.0
	github.com/json-iterator/go v1.1.9 // indirect
	github.com/klauspost/cpuid v1.2.1 // indirect
	go.mongodb.org/mongo-driver v1.5.1
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22 // indirect
)
